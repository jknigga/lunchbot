$(document).ready(function() {
    $('#name').editable({
        url: 'ajax/ajax-update-myinfo.php', //this url will not be used for creating new user, it is only for update
        mode: 'inline',
        success: function(data) {
            //alert(data);
        },
        error: function(errors) {
            //alert(errors);
        }
    });
    $('#email').editable({
        url: 'ajax/ajax-update-myinfo.php', //this url will not be used for creating new user, it is only for update
        mode: 'inline',
        success: function(data) {
            //alert(data);
        },
        error: function(errors) {
            //alert(errors);
        }
    });
    $('#birthday').editable({
        url: 'ajax/ajax-update-myinfo.php', //this url will not be used for creating new user, it is only for update
        mode: 'inline',
        format: 'YYYY-MM-DD',
        viewformat: 'MMMM D, YYYY',
        template: 'MMMM / D / YYYY',
        success: function(data) {
            //alert(data);
        },
        error: function(errors) {
           //alert(errors);
        }
    });
    $('#emailpref-1').editable({
        value: 1,
        source: [
            {value: 1,text: 'Yes please!'},
            {value: 2,text: 'No thanks'}
        ]
    });
    $('#emailpref-2').editable({
        value: 1,
        source: [
            {value: 1,text: 'Yes please!'},
            {value: 2,text: 'No thanks'}
        ]
    });
    $('#emailpref-3').editable({
        value: 1,
        source: [
            {value: 1,text: 'Yes please!'},
            {value: 2,text: 'No thanks'}
        ]
    });
    $('.rateit').bind('rated reset', function (e) {
        var ri = $(this);

        //if the use pressed reset, it will get value: 0 (to be compatible with the HTML range control), we could check if e.type == 'reset', and then set the value to  null .
        var value = ri.rateit('value');
        var locID = ri.data('locid'); // if the product id was in some hidden field: ri.closest('li').find('input[name="productid"]').val()
        var userID = ri.data('userid');

        //maybe we want to disable voting?
        //ri.rateit('readonly', true);

        $.ajax({
            url: 'ajax/ajax-update-rating.php', //your server side script
            data: { user: userID, loc: locID, value: value }, //our data
            type: 'POST',
            success: function (data) {
                //alert(data);
            },
            error: function (jxhr, msg, err) {
                //$('#response').append('<li style="color:red">' + msg + '</li>');
            }
        });
    });

    // admin things

    $('#adduser-form').on("submit", function(e){
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "ajax/ajax-admin.php",
            data: $("#adduser-form").serialize(), // serializes the form's elements.
            success: function(data) {
                $('.lunchbot-alert').addClass("alert alert-success").html(data).slideDown().delay(1500).slideUp();
            }
        });
    });

    $('#addlocation-form').on("submit", function(e){
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "ajax/ajax-admin.php",
            data: $("#addlocation-form").serialize(), // serializes the form's elements.
            success: function(data) {
                $('.lunchbot-alert').addClass("alert alert-success").html(data).slideDown().delay(1500).slideUp();
            }
        });
    });

    $('#removelocation-form').on("submit", function(e){
        e.preventDefault();
        var locname = $("#removelocation-id option:selected").text();
        $.ajax({
            type: "POST",
            url: "ajax/ajax-admin.php",
            data: $("#removelocation-form").serialize() + '&removelocation-name=' + locname, // serializes the form's elements.
            success: function(data) {
                $('.lunchbot-alert').addClass("alert alert-success").html(data).slideDown().delay(1500).slideUp();
            }
        });
    });

    $('.button-resetpw').on("click", function(e){
        e.preventDefault();
        var userid = $(this).prop('name');
        $.ajax({
            type: "POST",
            url: "ajax/ajax-admin.php",
            data: { action: "resetpw", id: userid },
            success: function(data) {
                $('.lunchbot-alert').addClass("alert alert-success").html(data).slideDown().delay(1500).slideUp();
            }
        });
    });

    $('.button-deleteuser').on("click", function(e){
        e.preventDefault();
        var userid = $(this).prop('name');
        $.ajax({
            type: "POST",
            url: "ajax/ajax-admin.php",
            data: { action: "deleteuser", id: userid },
            success: function(data) {
                $('.lunchbot-alert').addClass("alert alert-info").html(data).slideDown().delay(1500).slideUp();
            }
        });
    });

});