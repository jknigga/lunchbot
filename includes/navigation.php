<div class="row info-bar">
    <div class="user-welcome">
        <p>Hello <?=$_SESSION['user']?></p>
    </div>

    <div class="navigation">
        <ul>
            <li><a href="gui.php">Home</a></li>
            <li><a href="gui-history.php">History</a></li>
            <li><a href="gui-locations.php">Locations</a></li>
            <li><a href="gui-my-info.php">My Info</a></li>
            <?php if ($_SESSION['access'] == 1) { echo '<li class="admin-link"><a href="gui-admin.php">Admin</a></li>'; } ?>
            <li><a href="includes/do-logout.php">Logout</a></li>
        </ul>
    </div>
</div>