<?php

class DB {
    private static $mysqli;
    private function __construct(){} //no instantiation

    //public $db_server = "localhost";
    //public $db_port = "3306";
    //public $db_user = "jakobkni_fdi";
    //public $db_pass = "functional";
    //public $db_name = "jakobkni_lunchfdi";
    //public $test_db_name = "jakobkni_lunchfdi_test";

    static function cxn() {
        if( !self::$mysqli ) {
            self::$mysqli = new mysqli("localhost", "jakobkni_fdi", "functional", "jakobkni_lunchfdi", "3306");
        }
        return self::$mysqli;
    }
}