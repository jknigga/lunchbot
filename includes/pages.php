<?php

if(isset($_GET['a'])){ $action = $_GET['a']; }

$day_of_week = date('w');

?>

<div class="row col-md-12 text-center">
<?php

switch ($page) {
    case "index":
        if ($day_of_week == '5') { ?>
            <p class="text-center">Today's restaurant is <strong><?=get_last_location()?></strong>. Are you going to be attending?</p>
        <?php } else { ?>
            <p class="text-center">The last restaurant we went to was <strong><?=get_last_location()?></strong>. I wonder where we will go next!</p>
        <?php }
        break;
    case "history": ?>
        <h2>History</h2>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Date</th>
                <th>Restaurant</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $history = get_history();

            foreach ($history as $row) {
                echo '<tr>';
                    echo '<td>' . $row['date'] . '</td>';
                    echo '<td><a href="gui-view.php?p=' . $row['loc_id'] . '">' . $row['name'] . '</a></td>';
                echo '</tr>';
            }
            ?>
            </tbody>
        </table>
        <?php
        break;
    case "locations": ?>
        <h2>Locations</h2>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Location</th>
                <th>Visits</th>
                <th>My Rating</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $history = get_locations();

            foreach ($history as $row) {
                $rating = get_rating($row['id'],$_SESSION['id']);
                echo '<tr>';
                echo '<td><a href="gui-view.php?p=' . $row['id'] . '">' . $row['name'] . '</a></td>';
                echo '<td>' . $row['visits'] . '</td>';
                echo '<td><div data-rateit-value="'.$rating[0]['rating'].'" data-rateit-resetable="false" data-userid="'.$_SESSION['id'].'" data-locid="'.$row['id'].'" class="rateit"></div></td>';
                echo '</tr>';
            }
            ?>
            </tbody>
        </table>
        <?php
        break;
    case "info": ?>
        <h2>My Info</h2>
        <small>Click on info to edit it.</small>
        <!--email_off-->
        <table class="table table-myinfo table-striped">
        <tbody>
        <?php
        $userinfo = get_user_info($_SESSION['id']);

            echo '<tr>';
            echo '<td>Name</td><td><a href="#" class="x-editable" id="name" data-type="text" data-pk="' . $userinfo[0]['id'] . '">' . $userinfo[0]['name'] . '</a></td>';
            echo '</tr>';
            echo '<tr>';
            echo '<td>Email</td><td><a href="#" class="x-editable" id="email" data-type="text" data-pk="' . $userinfo[0]['id'] . '">' . $userinfo[0]['email'] . '</a></td>';
            echo '</tr>';
            echo '<tr>';
            echo '<td>Birthday</td><td><a href="#" class="x-editable" id="birthday" data-type="combodate" data-pk="' . $userinfo[0]['id'] . '" data-value="' . $userinfo[0]['birthday'] . '">' . $userinfo[0]['birthdayreadable'] . '</a></td>';
            echo '</tr>';
        ?>
        </tbody>
        </table>

        <h2>Email Preferences</h2>
        <small>Set your email preferences below.</small>

        <table class="table table-striped">
            <tr>
                <td>Weekly email when the location is selected</td>
                <td><a href="#" class="x-editable" id="emailpref-1" data-type="select" data-pk="1" data-url="/post" data-title="Select status"></a></td>
            </tr>
            <tr>
                <td>Location updates</td>
                <td><a href="#" class="x-editable" id="emailpref-2" data-type="select" data-pk="1" data-url="/post" data-title="Select status"></a></td>
            </tr>
            <tr>
                <td>Important Notifications</td>
                <td><a href="#" class="x-editable" id="emailpref-3" data-type="select" data-pk="1" data-url="/post" data-title="Select status"></a></td>
            </tr>
        </table>
        <!--/email_off-->
        <?php
        break;
    case "view":
        $location = get_location_details($_GET['p']);
        ?>

        <h2><?=$location[0]['name']?></h2>

        <div class="container">
            <div class="row col-md-4">
                <h3>Statistics</h3>
                <p>All I can think of is last visited, times visited... times... skipped?</p>
            </div>
            <div class="row col-md-4">

                <h3>Information</h3>

                <p>I am not sure what to put here quite yet, but I think something should go here.</p>

            </div>
            <div class="row col-md-4">
                <h3>Rating</h3>
                <table class="table">
                    <tr>
                        <td>Overall Rating</td>
                        <td>
                            <?php
                            $rating = get_rating($location[0]['id']);
                            echo '<div class="rateit" data-rateit-value="'.round($rating[0]['total_rating'],1).'" data-rateit-ispreset="true" data-rateit-readonly="true"></div>';
                            ?>
                        </td>
                    </tr>
                </table>

                </p>
            </div>
        </div>

        <?php
        break;
    case "admin": ?>
        <div class="lunchbot-alert"></div>
        <h2>Administration</h2>
        <div class="container text-left">
            <div class="row">
                 <div class="col-md-4">
                    <h3>Add User</h3>
                    <form class="form-horizontal" role="form" id="adduser-form">
                        <input type="hidden" id="action" name="action" value="adduser">
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="adduser-name">Name</label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="adduser-name" id="adduser-name" placeholder="Name"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="adduser-email">Email</label>
                            <div class="col-sm-10"><input type="email" class="form-control" name="adduser-email" id="adduser-email" placeholder="Enter email"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="adduser-access">Account Type</label>
                            <div class="col-sm-8">
                                <select class="form-control" name="adduser-access" id="adduser-access">
                                    <option value="0">User</option>
                                    <option value="2">Manager</option>
                                    <option value="1">Admin</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12"><button type="submit" class="btn btn-primary">Submit</button></div>
                    </form>
                </div>

                <div class="col-md-4">
                    <h3>Add Location</h3>
                    <div class="clearfix">
                        <form class="form-horizontal" role="form" id="addlocation-form">
                            <input type="hidden" id="action" name="action" value="addlocation">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="addlocation-name">Name</label>
                                <div class="col-sm-10"><input type="text" class="form-control" name="addlocation-name" id="addlocation-name" placeholder="Name"></div>
                            </div>
                            <div class="col-sm-12"><button type="submit" class="btn btn-primary">Submit</button></div>
                        </form>
                    </div>

                    <h3>Remove Location</h3>
                    <p>This will actually just initiate a vote to have the location removed.</p>
                    <div class="clearfix">
                        <form class="form-horizontal" role="form" id="removelocation-form">
                            <input type="hidden" id="action" name="action" value="removelocation">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="removelocation-id">Name</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="removelocation-id" id="removelocation-id">
                                        <?php
                                        $locations = get_locations();
                                        foreach($locations as $location) {
                                            echo '<option value="'.$location['id'].'">'.$location['name'].'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12"><button type="submit" class="btn btn-primary">Submit</button></div>
                        </form>
                    </div>
                </div>

                <div class="col-md-4">
                    <h3>User Tools</h3>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>User</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            $users = get_all_users($_SESSION['id']);
                            foreach($users as $user) {
                                echo '<tr>';
                                echo '<td>'.$user['name'].'</td>';
                                echo '<td>';
                                    echo '<button name="'.$user['id'].'" type="button" class="button-resetpw btn btn-primary btn-xs">Reset PW</button> ';
                                    echo '<button name="'.$user['id'].'" type="button" class="button-deleteuser btn btn-danger btn-xs">Delete</button>';
                                echo '</td>';
                                echo '</tr>';
                            }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <?php
        break;
}
?>
</div>