<?php
session_start();

require("db-connect.php");
require("password-hasher.php");

// Base-2 logarithm of the iteration count used for password stretching
$hash_cost_log2 = 8;
// Do we require the hashes to be portable to older systems (less secure)?
$hash_portable = FALSE;

$email = $_POST['email'];
$password = $_POST['password'];

$hasher = new PasswordHash($hash_cost_log2, $hash_portable);

function fail($pub, $pvt = '')
{
    $msg = $pub;
    if ($pvt !== '')
        $msg .= ": $pvt";
    exit("An error occurred ($msg).\n");
}

$hash = '*'; // In case the user is not found

($stmt = DB::cxn()->prepare('select id, name, password, access_level from users where email=?'))
    || fail('MySQL prepare', DB::cxn()->error);
$stmt->bind_param('s', $email)
    || fail('MySQL bind_param', DB::cxn()->error);
$stmt->execute()
    || fail('MySQL execute', DB::cxn()->error);
$stmt->bind_result($id, $user, $hash, $access)
    || fail('MySQL bind_result', DB::cxn()->error);

if (!$stmt->fetch() && DB::cxn()->errno)
    fail('MySQL fetch', DB::cxn()->error);

if ($hasher->CheckPassword($password, $hash)) {
    $_SESSION['user'] = $user;
    $_SESSION['id'] = $id;
    $_SESSION['access'] = $access;
} else {
    $what = 'Authentication failed';
}
unset($hasher);

$stmt->close();
DB::cxn()->close();

header("Location: /lunch-fdi/gui.php");