<?php

function check_login() {
    if(!$_SESSION['user']) {
        return 0;
    } else {
        return 1;
    }
}

function resultToArray($result) {
    $rows = array();
    while($row = $result->fetch_assoc()) {
        $rows[] = $row;
    }
    return $rows;
}

function get_last_location() {

    $location = '';

    ($stmt = DB::cxn()->prepare('SELECT l.name FROM history h INNER JOIN location l ON h.loc_id = l.id WHERE h.id = (SELECT max(id) FROM history)'))
    || fail('MySQL prepare', DB::cxn()->error);
    $stmt->execute()
    || fail('MySQL execute', DB::cxn()->error);
    $stmt->bind_result($location)
    || fail('MySQL bind_result', DB::cxn()->error);

    if (!$stmt->fetch() && DB::cxn()->errno)
        fail('MySQL fetch', DB::cxn()->error);

    return $location;
}

function get_history() {

    $history = DB::cxn()->query("SELECT history.id, history.loc_id, DATE_FORMAT(history.date, '%M %e, %Y') as date, location.name FROM history INNER JOIN location ON history.loc_id =location.id ORDER BY history.id DESC");
    $results_array = resultToArray($history);

    return $results_array;
}

function get_locations() {
    $locations = DB::cxn()->query("SELECT id, name, visits FROM location WHERE weight > 0;");
    $results_array = resultToArray($locations);

    return $results_array;
}

function get_location_details($id) {
    $location = DB::cxn()->query("SELECT * FROM location WHERE id = ".$id.";");
    $results_array = resultToArray($location);

    return $results_array;
}

function get_user_info($id) {
    $user = DB::cxn()->query("SELECT id, name, email, DATE_FORMAT(birthday, '%M %e, %Y') as birthdayreadable, birthday FROM users WHERE id = $id;");
    $results_array = resultToArray($user);

    return $results_array;
}

function get_rating($locid, $userid=0) {
    if ($userid == 0) {
        $ratings = DB::cxn()->query("SELECT AVG(rating) as total_rating, COUNT(rating) as count FROM ratings WHERE loc_id = '".$locid."';");
    } else {
        $ratings = DB::cxn()->query("SELECT rating FROM ratings WHERE loc_id = '".$locid."' AND user_id = '".$userid."';");
    }

    $results_array = resultToArray($ratings);

    return $results_array;
}

function show_message($message, $type = "info") {
    echo '<div class="alert alert-'.$type.'">'.$message.'</div>';
}

function get_all_users($me) {
    $users = DB::cxn()->query("SELECT id, name FROM users WHERE id != $me;");
    $results_array = resultToArray($users);

    return $results_array;
}