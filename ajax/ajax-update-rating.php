<?php
session_start();

require('../includes/db-connect.php');
/*
Script for update record from X-editable.
You will get 'pk', 'name' and 'value' in $_POST array.
*/
$user_id = mysqli_real_escape_string(DB::cxn(), $_POST['user']);
$loc_id = mysqli_real_escape_string(DB::cxn(), $_POST['loc']);
$value = mysqli_real_escape_string(DB::cxn(), $_POST['value']);

/*
 Check submitted value
*/
if(!empty($value)) {
    $test = DB::cxn()->query('SELECT rating FROM ratings WHERE loc_id = '.$loc_id.' AND user_id = '.$user_id);

    if($test->num_rows==0) {
        $result = DB::cxn()->query('INSERT INTO ratings SET rating = "'.$value.'", loc_id = "'.$loc_id.'", user_id = "'.$user_id.'";');
    } else {
        $result = DB::cxn()->query('UPDATE ratings SET rating = "'.$value.'" WHERE loc_id = '.$loc_id.' AND user_id = '.$user_id);
    }

} else {
    header('HTTP 400 Bad Request', true, 400);
    echo "This field is required!";
}

?>