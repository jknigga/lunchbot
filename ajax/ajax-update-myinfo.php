<?php
session_start();

require('../includes/db-connect.php');
/*
Script for update record from X-editable.
You will get 'pk', 'name' and 'value' in $_POST array.
*/
$pk = mysqli_real_escape_string(DB::cxn(), $_POST['pk']);
$name = mysqli_real_escape_string(DB::cxn(), $_POST['name']);
$value = mysqli_real_escape_string(DB::cxn(), $_POST['value']);

/*
 Check submitted value
*/
if(!empty($value)) {
    $result = DB::cxn()->query('UPDATE users SET '.$name.' = "'.$value.'" WHERE id = '.$pk);
    if ($name == "name") {
        $_SESSION['user'] = $value;
    }
} else {
    header('HTTP 400 Bad Request', true, 400);
    echo "This field is required!";
}

?>