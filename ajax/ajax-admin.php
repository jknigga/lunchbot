<?php
session_start();

require('../includes/db-connect.php');
/*
Script for update record from X-editable.
You will get 'pk', 'name' and 'value' in $_POST array.
*/

$action = mysqli_real_escape_string(DB::cxn(), $_POST['action']);

/*
 Check submitted value
*/
if(!empty($action)) {
    //$result = DB::cxn()->query('UPDATE users SET '.$name.' = "'.$value.'" WHERE id = '.$pk);
    switch ($action) {
        case "adduser":
            $name = mysqli_real_escape_string(DB::cxn(), $_POST['adduser-name']);
            $email = mysqli_real_escape_string(DB::cxn(), $_POST['adduser-email']);
            $access = mysqli_real_escape_string(DB::cxn(), $_POST['adduser-access']);
            echo "User <strong>".$name."</strong> was successfully added!";
            break;
        case "addlocation":
            $name = mysqli_real_escape_string(DB::cxn(), $_POST['addlocation-name']);
            echo "<strong>".$name."</strong> was added successfully!";
            break;
        case "removelocation":
            $name = mysqli_real_escape_string(DB::cxn(), $_POST['removelocation-name']);
            $id = mysqli_real_escape_string(DB::cxn(), $_POST['removelocation-id']);
            echo "Vote to remove <strong>".stripslashes($name)."</strong> has been started.";
            break;
        case "resetpw":
            $id = mysqli_real_escape_string(DB::cxn(), $_POST['id']);
            echo "User (".$id.") password has been reset.";
            break;
        case "deleteuser":
            $id = mysqli_real_escape_string(DB::cxn(), $_POST['id']);
            echo "User (".$id.") deleted successfully.";
            break;
    }

} else {
    header('HTTP 400 Bad Request', true, 400);
    echo "Ooops";
}

?>