<?php



require("includes/db-connect.php");

require("includes/password-hasher.php");



// Base-2 logarithm of the iteration count used for password stretching

$hash_cost_log2 = 8;

// Do we require the hashes to be portable to older systems (less secure)?

$hash_portable = FALSE;



$action = $_GET['a'];



function fail($pub, $pvt = '')

{

    $msg = $pub;

    if ($pvt !== '')

        $msg .= ": $pvt";

    exit("An error occurred ($msg).\n");

}



if ($action == "new") {



    $email = $_POST['email'];

    $password = $_POST['password'];



    $hasher = new PasswordHash($hash_cost_log2, $hash_portable);

    $hash = $hasher->HashPassword($password);

    if (strlen($hash) < 20)

        fail('Failed to hash new password');

    unset($hasher);



    ($stmt = DB::cxn()->prepare('update users set password = ? where email = ?'))

    || fail('MySQL prepare', DB::cxn()->error);

    $stmt->bind_param('ss', $hash, $email)

    || fail('MySQL bind_param', DB::cxn()->error);

    $stmt->execute()

    || fail('MySQL execute', DB::cxn()->error);



    $stmt->close();

    DB::cxn()->close();



    header("Location: /lunch-fdi/make-user.php");



} else { ?>



    <html>

    <head>

        <title>sup</title>

    </head>

    <body>

    <form action="make-user.php?a=new" method="POST">

        Email:<br>

        <input type="text" name="email" size="60"><br>

        Password:<br>

        <input type="password" name="password" size="60"><br>

        <input type="submit" value="Create user">

    </form>

    </body>

    </html>



<?php } ?>