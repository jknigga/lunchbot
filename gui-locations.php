<?php
session_start();

require('includes/db-connect.php');
require('includes/functions.php');

$page = "locations";
?>

<!doctype html>
<html>
<head>
    <?php include('includes/head.php'); ?>
</head>
<body>
<div class='jumbotron intro'>
    <div class="container">
        <h1>Lunchbot</h1>
        <p>Functional Devices Edition</p>
    </div>
</div>

<div class='container'>
        <?php
            if (check_login() == 0) {
                include("includes/login-form.php");
            } else {
                include("includes/navigation.php");
                include("includes/pages.php");
            }
        ?>
    </div>

<?php include('includes/footer.php'); ?>